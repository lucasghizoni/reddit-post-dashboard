## Reddit post Dashboard

To run this project, please install node v12.13.0 or higher

### Installing dependencies

`npm install`

## Npm Scripts

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### Testing error on fetch API

To test how UI behaves with error on fetching posts open [http://localhost:3000?withError](http://localhost:3000?withError)

### `npm test`

Run tests

### Possible Improvements

- Create more unit tests, also for the components, and not only for actions
- Use CSS in JS instead of pure .css files.
- Improve the recursive logic for rendering nested comments.
- Improve responsive behaviour also for fonts and paddings.