import React, { useEffect } from 'react';
import './App.css';
import { Header } from './components/Header/Header';
import { UserInfo } from './components/UserInfo/UserInfo';
import { useAppContext } from './context-api/AppProvider';
import { PostList } from './components/PostList/PostList';

export const App = () => {
  const { fetchUser, fetchPosts } = useAppContext().actions;
  
  useEffect(() => {
    fetchUser();
    fetchPosts();
  }, []);

  return (
    <div className="App">
      <Header>
        <UserInfo/>
      </Header>
      <section className="App-section">
        <PostList/>
      </section>
    </div>
  );
}
