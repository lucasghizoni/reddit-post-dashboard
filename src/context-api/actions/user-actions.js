export const fetchUserAction = async () => {
  const res = await fetch('/user.json');
  return res.json();
};