import { removePostCommentAction } from './post-actions';

const posts = [
  {
    comments: [
      { parent_id: 232321, id: 1 }, 
      { parent_id: 1, id: 11 }, 
      { parent_id: 1, id: 111 }, 
      { parent_id: 111, id: 333 }, 
      { parent_id: 4, id: 44 }
    ]
  }
];

it('Remove comment without children', () => {
  const expectedComments = [
    { id: 333, parent_id: 111 },
    { id: 11, parent_id: 1 }, 
    { id: 111, parent_id: 1 },
    { id: 1, parent_id: 232321 }
  ];

  const updatedPosts = removePostCommentAction(posts, 0, 44);
  expect(updatedPosts[0].comments).toEqual(expect.arrayContaining(expectedComments));
});

it('Remove comment with children', () => {
  const expectedComments = [
    { parent_id: 4, id: 44 }
  ];

  const updatedPosts = removePostCommentAction(posts, 0, 1);
  expect(updatedPosts[0].comments).toEqual(expect.arrayContaining(expectedComments));
});