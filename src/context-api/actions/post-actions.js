const simulatingError = window.location.search.indexOf('withError') !== -1;

export const fetchPostsAction = async () => {
  const url = simulatingError ? '/api/postswrongpath' : '/api/posts';
  const res = await fetch(url);
  return res.json();
}

export const removePostCommentAction = (posts, postIndex, commentId) => {
  const post = posts[postIndex];

  const comments = removeChildrenComments(post.comments, commentId);

  return [
    ...posts.slice(0, postIndex),
    { 
      ...post, 
      comments 
    },
    ...posts.slice(postIndex + 1)
  ]
};

const removeChildrenComments = (comments, id) => {
  const map = mapByParentId(comments, id);

  recursiveDelete(map, [id]);

  const updatedComments = parseMapValuesToArray(map);

  return updatedComments.filter(c => c.id !== id); // removing the root comment.
};

const recursiveDelete = (map, ids) => {
  ids.forEach( id => {
    const children = map.get(id);
    map.delete(id);
    
    if(children){
      recursiveDelete(map, children.map(c => c.id));
    }
    
  });
};

const mapByParentId = (comments, id) => {
  const map = new Map();

  comments.forEach(comment => {
    let key = 'noParent';

    if(comment.parent_id) {
      key = comment.parent_id;
    }

    const values = map.get(key);

    let content = [comment];
    if(values) {
      content = [...values, comment];
    }
    
    map.set(key, content);
  });
  return map;
};

const parseMapValuesToArray = map => {
  let arr = [];
  
  for (const [k, v] of map.entries()) {
    arr = [...v, ...arr];
  }
  return arr;
};