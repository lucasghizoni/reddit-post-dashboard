import React, { useState, createContext, useContext, useMemo } from 'react';
import { fetchUserAction } from './actions/user-actions';
import { fetchPostsAction, removePostCommentAction } from './actions/post-actions';

const Context = createContext();

export const AppProvider = props => {
  const [state, setState] = useState({
    user: {},
    posts: [],
    fetchPostsError: false
  });
  
  const context = useMemo(() => {
    return {
      state,
      actions: {
        fetchUser: async () => {
          const user = await fetchUserAction();
          setState(prevState => ({
            ...prevState, 
            user
          }));
        },
        fetchPosts: async () => {
          let posts;
          try {
            posts = await fetchPostsAction();
            setState(prevState => ({
              ...prevState,
              posts
            }));
          } catch (error) {
            setState(prevState => ({
              ...prevState,
              fetchPostsError: true
            }));
          }
        },
        removePostComment: (postIndex, commentId) => {
          const posts = removePostCommentAction(state.posts, postIndex, commentId);
          setState({
            ...state,
            posts
          });
        }    
      }
    }
  }, [state]);

  return (
    <Context.Provider 
      value={context}
    >
      {props.children}
    </Context.Provider>
  );
};

export const useAppContext = () => useContext(Context);