const proxy = require("http-proxy-middleware");

module.exports = function (app) {

  app.use(
    "/api/posts",
    proxy({
      target: "https://aud-tech-challenge.s3.eu-central-1.amazonaws.com/challenge.json",
      changeOrigin: true,
      pathRewrite: {
          "^/api/posts": ""
      }
    }),
  );
};