export const  germanFormat = timestamp => {
  const date = new Date(timestamp * 1000);
  
  const month = date.getMonth() + 1;
  const day = date.getDate();

  const datePart = `${addNecessaryZero(day)}.${addNecessaryZero(month)}.${date.getFullYear()}`;
  const timePart = `${addNecessaryZero(date.getHours())}:${addNecessaryZero(date.getMinutes())}`;
  return `${datePart} at ${timePart}`;
};

const addNecessaryZero = value => {
  return value > 9 ? value : '0' + value;
};