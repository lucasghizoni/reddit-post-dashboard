import React from 'react';
import './CommentList.css';
import { Comment } from '../Comment/Comment';

const sortComments = comments => 
  comments.sort((a, b) =>  b.created_utc - a.created_utc);

const renderChildrenComments = (id, comments, postIndex) => {
  const filteredComments = comments.filter(c => c.parent_id === id);
  const sortedComments = sortComments(filteredComments);

  return (
    <>
      {sortedComments.map(comment => {
        return (
          <Comment
            key={comment.id}
            postIndex={postIndex} 
            comment={comment}
          >
            {renderChildrenComments(comment.id, comments, postIndex)}
          </Comment>
        );
      })}
    </>
  );
};

export const CommentList = props => {
  const { postIndex, comments } = props;
  const rootComments = [];
  const allChildrenComments = [];
  const sortedComments = sortComments(comments);

  sortedComments.forEach(comment => {
    if(comment.parent_id) {
      allChildrenComments.push(comment);
      return;
    }
    rootComments.push(comment);
  });

  return (
    <div className="CommentList">
      {rootComments.map(comment => 
        <Comment
          key={comment.id}
          postIndex={postIndex} 
          comment={comment}
        >
          {renderChildrenComments(comment.id, allChildrenComments, postIndex)}
        </Comment>
      )}
    </div>
  );
};