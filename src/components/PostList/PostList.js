import React, { useState } from 'react';
import { IoMdEye } from 'react-icons/io';
import './PostList.css';
import { Post } from '../Post/Post';
import { useAppContext } from '../../context-api/AppProvider';

const PAGE_SIZE = 4;

export const PostList = () => {
  const [ limit, setLimit ] = useState(PAGE_SIZE);
  const { posts, fetchPostsError } = useAppContext().state;
  
  const slicedPosts = posts.slice(0, limit);
  const showBtnMore = slicedPosts.length >= limit; 

  return (
    <div className="PostList">
      {!fetchPostsError && 
        <>
          <div className="PostList-container">
            {slicedPosts.map((post, i) =>
              <Post key={post.id} post={post} postIndex={i}/>
            )}
          </div>
          {showBtnMore &&
            <div className="PostList-footer">
              <button onClick={() => setLimit( limit + PAGE_SIZE )} className="PostList-btnMore">
                <IoMdEye className="PostList-IconMore"/>
                SEE MORE
              </button>
            </div>
          }
        </>
      }
      {fetchPostsError && 'Error while trying to load posts. Try again later.'}
    </div>
  );
}