import React from 'react';
import './Comment.css';
import { MdDelete } from 'react-icons/md';
import { IoMdArrowDropdown, IoMdArrowDropup } from 'react-icons/io';
import { useAppContext } from '../../context-api/AppProvider';
import { germanFormat } from '../../helper/date-helper';


export const Comment = props => {
  const { removePostComment } = useAppContext().actions;
  
  const { comment, postIndex } = props;

  const hasChildren = props.children.props.children.length > 0;

  const handleClickDel = () => {
    removePostComment(postIndex, comment.id);
  }

  const dateFormatted = germanFormat(comment.created_utc);

  return (
    <div className="Comment">
      <div className="Comment-summary" title={`${comment.author} - ${dateFormatted}`}>
        <span className="Comment-author">{comment.author}</span> - {dateFormatted} <MdDelete onClick={handleClickDel} className="Comment-deleteIcon"/>
      </div>
      <div className="Comment-content">
        <div className="Comment-arrowsContainer">
        {hasChildren &&
          <div className="Comment-arrows">
            <IoMdArrowDropup/>
            <IoMdArrowDropdown className="Comment-arrowDown"/>
          </div>
        }
        </div>
        <div title={comment.body} className="Comment-text">
          {comment.body}
        </div>
      </div>
      <div className="Comment-innerComments">
        {hasChildren &&
          <div className="Comment-verticalLine"/>
        }
        <div className="Comment-children">
          {props.children}
        </div>

      </div>
    </div> 
  );
};