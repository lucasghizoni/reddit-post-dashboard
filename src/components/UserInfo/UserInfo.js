import React from 'react';
import './UserInfo.css';
import { useAppContext } from '../../context-api/AppProvider';

export const UserInfo = () => {
  const { user } = useAppContext().state;

  return (
    <div className="UserInfo">
      <div className="UserInfo-avatar">
        <img alt="Profile avatar" src={user.avatarUrl}/>
      </div>
      <div className="UserInfo-container">
        <div className="UserInfo-name">
          {user.name}
        </div>
        <div className="UserInfo-title">
          {user.title}
        </div>
      </div>
    </div>
  );
};