import React, { useState } from 'react';
import './Post.css';
import { MdModeComment, MdExpandMore, MdExpandLess } from 'react-icons/md';
import { CommentList } from '../CommentList/CommentList';

export const Post = props => {
  const [showComments, setShowComments] = useState(false); 
  const { post, postIndex } = props;

  const hasComments = post.comments.length > 0;

  const imgSrc = 
    post.thumbnail === "self" || post.thumbnail === "default" ? 
      "/fallback.jpg" : 
      post.thumbnail;

  const titleStyl = {
    WebkitLineClamp: post.selftext ? 1 : 3
  };

  return (
    <div className="Post">
      <div className="Post-summary" onClick={() => setShowComments(hasComments && !showComments)}>
        <div className="Post-imgContainer">
          <img className="Post-img" src={imgSrc}/>
        </div>
        <div className="Post-container">
          <div title={post.title} style={titleStyl} className="Post-title">
            {post.title}
          </div>
          <div title={post.selftext} className="Post-content">
            {post.selftext}
          </div>
          {hasComments && 
            <div className="Post-footer">
              <MdModeComment/> 
              {post.comments.length} 
              {showComments && <MdExpandLess/>}
              {!showComments && <MdExpandMore/>}
            </div>
          }
        </div>
      </div>
      {showComments &&
        <>
          <br/>
          <CommentList 
            comments={post.comments} 
            postIndex={postIndex} 
          />
        </>
      }
    </div>
  );
};