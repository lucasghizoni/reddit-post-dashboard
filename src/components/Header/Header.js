import React from 'react';
import './Header.css';

export const Header = props => {

  return (
    <>
      <header className="Header Header-height">
        {props.children}
      </header>
      <div className="Header-height"/>
    </>
  );
};