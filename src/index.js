import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from './App';
import { AppProvider } from './context-api/AppProvider';

const AppWithContext = () => (
  <AppProvider>
    <App/>
  </AppProvider>
);

ReactDOM.render(<AppWithContext />, document.getElementById('root'));
